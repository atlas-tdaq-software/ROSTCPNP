package ROSTCPNP

author mikael.stefan.kvist@cern.ch
manager mikael.stefan.kvist@cern.ch

private
#----------------------------------------------------------
use ROSDescriptorNP
use ROSUtilities
use ROSCore
use ROOT * DQMCExternal
use monsvc
use is
use tbb
use genconfig

macro           generate-config-include-dirs        "${TDAQ_INST_PATH}/share/data/dal ${TDAQ_INST_PATH}/share/data/DFConfiguration"
document        generate-config DFdal               -s=../schema/ \
                                                    namespace="daq::df" \
                                                    include="DFdal" \
                                                    TCPNP.schema.xml

macro           gdir                                "$(bin)DFdal.tmp"

library         TCPNP-dal                           "$(gdir)/*.cpp"

macro TCPNP-dal_dependencies                        "DFdal"

macro_append TCPNP-dal_shlibflags                   "-ldaq-core-dal -ldaq-df-dal"

library TCPNPModule                                 "TCPNPAlloc.cxx \
                                                     TCPNPDescriptorModule.cxx"

macro TCPNPModule_dependencies                      "TCPNP-dal TCPNPInfo"
macro_append TCPNPModule_shlibflags                 "-lNPDescriptor -lTCPNP-dal -leformat"

document is-generation TCPNPInfo \
                    -s=../schema \
                 namespace="ROS" \
            header_dir="ROSInfo" \
            TCPNPInfo.schema.xml

public
#----------------------------------------------------------
apply_pattern install_libs files="libTCPNP-dal.so \
                                  libTCPNPModule.so"

apply_pattern install_db_files  name=schema target_dir="schema" \
                                            files="../schema/TCPNP.schema.xml"
apply_pattern install_data  name=is_schema src_dir="../schema" files="TCPNPInfo.schema.xml"

# IS classes
apply_pattern   install_headers name=is_info            src_dir="$(bin)/ROSInfo" \
                      		         		files="*.h"              \
		                      		        target_dir="../ROSInfo"
macro sw.repository.is-info-file.share/data/ROSTCPNP/TCPNPInfo.schema.xml:name \
                         "ROS TCPNPDescriptor IS xml description"