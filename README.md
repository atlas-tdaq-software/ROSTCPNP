__README__

ROS Network Input Module
  
The Readout team has launched an effort to replace legacy calibration modes.
The long term goal is to converge to a limited number of software operation
schemes for the ROS. This will reduce the maintenance and debugging efforts,
while providing a more stable and effective support for detector calibration.
In order to cover all the use cases, a new network-based input module for the
ROS has to be developed. Mikael will be in charge of this development, which
is expected to be fully functional and tested for inclusion in the end of year
release (November).


__MAINTAINERS__

Current maintainer(s):
 * Mikael Kvist - mikael.stefan.kvist@cern.ch


__FAQ__

 1. I'm currently developing a new network-based plugin module. The idea is to
support retrieval of detector calibration data over TCP. I have used code from
*EmulatedNPDescriptorModule.cxx* [1] to implement a first working version. To test
this I have created a python script that sends ROD-fragments over TCP on one port.
The Level1ID is incremented by one for each new generated fragment. However, this
module is restricted to only support one channel and without loss of TCP data (no
loss of Level1IDs).
__I am not sure what you are trying to do with multiple channels. Are you sending
data from multiple network sources? With tcp, there should be no loss of data as
the tcp protocol will retry if any packets are lost by the network. Maybe you
mean you want to be resilient against the sender missing a trigger and just not
sending a packet for a particular L1 Id? In this case, when the fragment is
requested, you should create an empty fragment with the ROB header status set to
```0x20000000``` (lost) or ```0x40000000``` (pending) depending on whether you
have received fragments with a later Id or not.__

 2. How should ```m_collectorQueue->push()``` be used in a multiple channel system?
__You should only push to ```m_collectorQueue``` when you have processed the last
channel.__

 3. It seems that for every channelIndex there's a status, 
```NEW_REQUEST,REQUEST_PENDING``` and ```REQUEST_COMPLETE```. For example:
```descriptor->status(channelIndex,NPRequestDescriptor::RequestStatus::REQUEST_COMPLETE);``` [2]
What is their purpose and when should they be used?
__The request status in the descriptor was used by the request management to
handle retries. We don't currently use this mechanism as we don't issue the request
until we know the data have arrived or we have time out. The descriptor should
have status ```NEW_REQUEST``` for every channel when it is passed to the module's
```requestFragment()```. You should set it to ```REQUEST_COMPLETE``` for each
channel that you have supplied data for. For channels which you have generated an
empty fragment with status ```0x40000000```, you can set the request status to
```REQUEST_PENDING``` but we since we don't do re-requests at the moment, so you
could set these to ```REQUEST_COMPLETE``` as well.__

 4. What would be the best way to modify my python script to test multiple channels
and data losses? Example: What do you think about implementing two threads in the
script where one sends even and the other one sends odd Level1IDs?
__If you need multiple channels, I would have a thread for each sending (the
same or different) data to a different port. For missing L1 Ids, I would just have
the threadsthrow a random number and if it's less than some threshold, skip an event__

 5. In ```getL1CountPointers()``` [3] we are populating the countPointers with
```&m_lastl1```. As I understand it, in a multi-channel system we need one
```m_lastl1``` for each channel. So far so good. In *EmulatedNPDescriptorModule.cxx*
the ```m_lastl1``` has a fixed value, and in the TCP-module, I guess ```m_lastl1```
can not. Am I supposed to increment ```m_lastl1``` when a new fragment is received
for that channel? And if so, should ```m_lastl1``` have starting value zero.
__In a proper multichannel setup, you will have to keep an index for each channel
so it is best to have a class for the channel and have an ```m_lastl1``` as a
member of that class rather than the module class. The variable should be
initialised to ```0xffffffff``` as the ```NPRequestManager()``` sees this as a
'not started' flag.__

 6. I have a question about the dump and user function in *EmulatedNPDescriptorModule.cxx*,
what do they do and when are they called? (I can see that the dump is called by the user
-function). In my current TCP implementation I have left dump and user "empty".
__You do not need to implement or even declare either of these functions unless you
have something specific you want to do. The dump function is just there for debugging.
The user function is part of the run control interface. You can implement anything you
want in there and it will be called whenever a user command is sent from eg. ```rc_sender```.__


__REFERENCES__

 1. ^ https://gitlab.cern.ch/atlas-tdaq-software/ROSEmulatedNP/tree/master/src
 2. ^ https://gitlab.cern.ch/atlas-tdaq-software/ROSEmulatedNP/blob/master/src/EmulatedNPDescriptorModule.cxx#L427
 3. ^ https://gitlab.cern.ch/atlas-tdaq-software/ROSEmulatedNP/blob/master/src/EmulatedNPDescriptorModule.cxx#L248