// https://gitlab.cern.ch/vandelli/EDIT/blob/master/indexbuilding/receiver.cxx
template <class C>
DataServer<C>::~DataServer()
{
    for (auto acceptor: m_acceptors) {
        acceptor->close ();
        delete acceptor;
    }
    m_acceptors.clear ();
    m_numfds = 0;
}

template <class C>
void DataServer<C>::listenOn (tcp::endpoint const& endpoint)
{
    tcp::acceptor* acceptor = new tcp::acceptor (m_io_service);
    // Our listening point
    acceptor->open (endpoint.protocol ());
    // Black magic to avoid errors if a new
    // instance is started immediately after a previous one
    acceptor->set_option (tcp::acceptor::reuse_address (true));
    // Listen for incoming connections
    acceptor->bind (endpoint);
    acceptor->listen ();

    m_acceptors.push_back (acceptor);
    m_numfds ++;
}

template <class C>
void DataServer<C>::run ()
{
    m_thread = std::make_shared<std::thread>(&DataServer::work, this);
}

template <class C>
void DataServer<C>::stop ()
{
    // Stop accepting new connections
    m_running = false;
    m_thread->join();
}

template <class C>
void DataServer<C>::work ()
{
    std::vector<tcp::acceptor*>::size_type i;
    // Timeout for poll operation
    // If the value of timeout is 0, poll() shall
    // return immediately.
    int poll_timeout_ms = 1;

    struct pollfd* events = new struct pollfd[m_numfds];
    memset (events, '\0', m_numfds * sizeof *events);

    for (i = 0; i != m_numfds; i++) {
        events[i].events |= POLLIN;
        events[i].fd = m_acceptors[i]->native_handle ();
    }

    m_running = true;

    while (m_running) {
        // With poll we check if data are available
        // before reading
        // This is needed to avoid blocking forever
        poll (events, m_numfds, poll_timeout_ms);
        // Use indexes
        for (i = 0; i != m_numfds; i++) {
            // The field revents is an output parameter, filled by
            // the kernel with the events that actually occurred
            if (events[i].revents & POLLIN) {
                tcp::socket* socket = new tcp::socket (m_io_service);
                // Data are available. This is the server
                // so we expect this is in a new incoming connection
                // We accept it and spawn and handler for it
                m_acceptors[i]->accept (*socket);
                // Friendship ... ?
                // Add a new input connection to the list
                // of connections
                m_who->m_sockets.push_back (socket);
            }
        }
    }

    delete[] events;
}

// EOF
