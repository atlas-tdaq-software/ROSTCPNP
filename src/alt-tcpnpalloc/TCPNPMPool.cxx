#include<iostream>
#include<stdlib.h>
#include"TCPNPMPool.h"
using namespace ROS;
// !THIS MEMORY POOL HAS BEEN WRITTEN TO SUIT THE NEEDS OF THE TCP PLUGIN MODULE

// Constructor of this class. It allocates memory from the system and creates a
// static double linked list to manage all memory nodes.
TCPNPMPool::TCPNPMPool(unsigned long numOfElements,
                       unsigned long sizeOfEachElement) :
    m_allocatedList(NULL), m_freeList(NULL)
{
    for (unsigned long i = 0; i < numOfElements; i++) {
        // Allocate memory for node
        node_t* node = resizeNode(NULL, sizeOfEachElement);
        push(&m_freeList, node);
    }
}

// Destructor of this class. It's task is to delete the memory pool.
TCPNPMPool::~TCPNPMPool()
{
    node_t* node,* cursor;

    m_subscriptionList.clear();

    for (cursor = node = m_allocatedList; node; cursor = node) {
        node = node->next;
        delete[] cursor;
    }

    for (cursor = node = m_freeList; node; cursor = node) {
        node = node->next;
        delete[] cursor;
    }
}

// To allocate a memory node. If the fixed-size blocks allocation cannot
// provide a proper memory node, It will call new function instead.
void* TCPNPMPool::alloc (unsigned long size)
{
    if (!m_freeList) {
        return NULL;
    }

    node_t* node = deleteNode (&m_freeList, m_freeList);

    node = resizeNode(node, size);

    push(&m_allocatedList, node);

    if (!m_freeList) {
        notifySubscribers(!m_freeList);
    }

    return node + 1;
}

// To free a memory node. If the pointer points to a memory node, then insert it
// to Free linkedlist. Otherwise, call delete function.
void TCPNPMPool::dispose (void* p)
{
    if (!p) {
        return ;
    }

    bool save = !m_freeList;

    node_t* node = (node_t *)p - 1;

    deleteNode(&m_allocatedList, node);

    push(&m_freeList, node);

    if (save) {
        notifySubscribers(!m_freeList);
    }
}

void TCPNPMPool::addSubscriber(Subscriber client)
{
    m_subscriptionList.push_back(client);

    (client)(!m_freeList);
}

void TCPNPMPool::notifySubscribers(bool poolIsFull)
{
    for (auto client : m_subscriptionList) {
        (client)(poolIsFull);
    }
}

// Function to insert a node at the beginning of the doubly linkedlist.
void TCPNPMPool::push(node_t** lList, node_t* newNode)
{
    // Since we are adding at the beginning, prev is always NULL.
    newNode->prev = NULL;

    // Link the old list off the new node.
    newNode->next = *lList;

    // Change prev of head node to new node.
    if (*lList != NULL) {
        (*lList)->prev = newNode;
    }

    // Move the head to point to the new node.
    *lList = newNode;
}

// Function to delete a node in a Doubly linkedlist.
// lList --> pointer to head node pointer.
// del   --> pointer to node to be deleted.
TCPNPMPool::node_t* TCPNPMPool::deleteNode(node_t** lList, node_t* del)
{
    // If node to be delete is head node
    if (*lList == del) {
        *lList = del->next;
    }

    // Change next only if node to be deleted is NOT the last node.
    if (del->next != NULL) {
        del->next->prev = del->prev;
    }

    // Change prev only if node to be deleted is NOT the first node.
    if (del->prev != NULL) {
        del->prev->next = del->next;
    }

    return del;
}

// This function is similar to realloc().
TCPNPMPool::node_t* TCPNPMPool::resizeNode(node_t* node,
                                           unsigned long newCapacity)
{
    if (newCapacity == 0)
    {
        delete[] node;
        return NULL;
    }
    else if (!node)
    {
        node_t* newNode = new node_t[sizeof(node_t) + newCapacity];
        newNode->capacity = newCapacity;
        return newNode;
    }
    else if (newCapacity <= node->capacity)
    {
        return node;
    }
    else
    {
        node_t* newNode = new node_t[sizeof(node_t) + newCapacity];
        newNode->capacity = newCapacity;
        delete[] node;
        return newNode;
    }
}

// EOF
