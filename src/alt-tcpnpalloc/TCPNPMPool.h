#ifndef TCPNPMPOOL_H
#define TCPNPMPOOL_H
#include<list>
#include<functional>

namespace ROS {
    typedef std::function<void(bool)> Subscriber;

    class TCPNPMPool {
        // The purpose of the structure's definition is that we can operate
        // linkedlist conveniently.
        typedef struct node_s {
            struct node_s* prev,* next;
            unsigned long capacity;
        } node_t;

        // Manage all nodes with two linkedlists.
        //   1) Head pointer to allocated linkedlist.
        node_t* m_allocatedList;
        //   2) Head pointer to free linkedlist.
        node_t* m_freeList;

        std::list<Subscriber> m_subscriptionList;

        // Utility functions
        void push(node_t** lList, node_t* newNode);
        node_t* deleteNode(node_t** lList, node_t* del);
        node_t* resizeNode(node_t* node, unsigned long newCapacity);
        void notifySubscribers(bool poolIsFull);

    public:
        TCPNPMPool(unsigned long numOfElements,
                   unsigned long sizeOfEachElement);

        ~TCPNPMPool();

        void* alloc(unsigned long size);

        void dispose (void* p);

        void addSubscriber(Subscriber client);
    };
}

#endif//TCPNPMPOOL_H
