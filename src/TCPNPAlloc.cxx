#include<iostream>
#include<stdlib.h>
#include"ROSTCPNP/TCPNPAlloc.h"
// ! THIS FIXED-SIZE BLOCKS ALLOCATION HAS BEEN WRITTEN TO SUIT THE NEEDS OF THE TCP PLUGIN MODULE.

using namespace ROS;
// Constructor of this class. It allocates memory from the system and creates
// a static double linked list to manage all memory nodes.
TCPNPAlloc::TCPNPAlloc (unsigned long numOfElements, unsigned long sizeOfEachElement) :
    m_memStart (NULL), m_allocatedList (NULL), m_freedList (NULL),
    m_sizeOfMem (numOfElements * (sizeOfEachElement + sizeof (node_t))),
    m_sizeOfEachElement (sizeOfEachElement)
{   // Allocate a memory block.
    m_memStart = new char[m_sizeOfMem];

    for (unsigned long i = 0; i < numOfElements; i++) {
        node_t* curr = (node_t *)(m_memStart + i *(sizeOfEachElement + sizeof (node_t)));
        push (&m_freedList, curr);
    }
}

// Function to delete a node in a Doubly linkedlist.
// lList --> pointer to head node pointer.
// del   --> pointer to node to be deleted.
TCPNPAlloc::node_t* TCPNPAlloc::deleteNode (node_t** lList, node_t* del)
{   // If node to be delete is head node
    if (*lList == del)
        *lList = del->next;

    // Change next only if node to be deleted is NOT
    // the last node.
    if (del->next != NULL)
        del->next->prev = del->prev;

    // Change prev only if node to be deleted is NOT
    // the first node.
    if (del->prev != NULL)
        del->prev->next = del->next;

    return del;
}

// Function to insert a node at the beginning of the doubly linkedlist.
void TCPNPAlloc::push (node_t** lList, node_t* newNode)
{   // Since we are adding at the beginning,
    // prev is always NULL.
    newNode->prev = NULL;

    // Link the old list off the new node.
    newNode->next = *lList;

    // Change prev of head node to new node.
    if (*lList != NULL)
        (*lList)->prev = newNode;

    // Move the head to point to the new node.
    *lList = newNode;
}

// To allocate a memory node. If the fixed-size blocks allocation cannot
// provide a proper memory node, It will call new function instead.
void* TCPNPAlloc::alloc (unsigned long size)
{
    std::lock_guard<std::mutex> lock(m_mtx);
    if (size > m_sizeOfEachElement || m_freedList == NULL)
        return malloc (size);
    // FreeList isn't empty. Get a node from free linkedlist.
    node_t* curr = deleteNode (&m_freedList, m_freedList);
    push (&m_allocatedList, curr);

    return curr + 1;
    // m_mtx is automatically released when lock
    // goes out of scope.
}

// To free a memory node. If the pointer points to a memory node, then insert it
// to Free linkedlist. Otherwise, call delete function.
void TCPNPAlloc::dispose (void* p)
{
    std::lock_guard<std::mutex> lock(m_mtx);
    if (m_memStart < p && p < (m_memStart + m_sizeOfMem)) {
        node_t* curr = (node_t *)p - 1;
        deleteNode (&m_allocatedList, curr);
        (void)push (&m_freedList, curr);
    }
    else free (p);
}
