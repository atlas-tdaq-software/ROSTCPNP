// 3) ROS Network Input Module
// The Readout team has launched an effort to replace legacy calibration modes.
// The long term goal is to converge to a limited number of software operation
// schemes for the ROS. This will reduce the maintenance and debugging efforts,
// while providing a more stable and effective support for detector calibration.
// In order to cover all the use cases, a new network-based input module for the
// ROS has to be developed. Mikael will be in charge of this development, which
// is expected to be fully functional and tested for inclusion in the end of year
// release (November).
#include<fstream>
#include<iterator>
#include"ROSTCPNP/TCPNPDescriptorModule.h"
#include"DFdal/RobinDataChannel.h"
#include"DFdal/TCPNPModule.h"
#include"DFDebug/DFDebug.h"
#define STATUS_LOST     (0x20000000)
#define STATUS_PENDING  (0x40000000)
// For ROD fragments, the Major version number is 3.1 and the Minor version is
// free to be chosen by the specific Sub Detector groups.
#define MAJOR_VERSION   (0x0301)
#define MINOR_VERSION   (0x0000)
#define FMT_VERSION_NUM (MAJOR_VERSION << 16 | MINOR_VERSION) // Format version number
#define NUMBER_OF_DATA_ELEMENTS (RODFragment::s_rodheaderSize + RODFragment::s_rodtrailerSize)
// KiB, MiB, and GiB, denote 1024 bytes, 1048576 bytes,
// and 1073741824 bytes.
#define ONE_KiB 1024
#define ONE_MiB (ONE_KiB * ONE_KiB)
#define ONE_GiB (ONE_MiB * ONE_KiB)
#define BINARY_PREFIX(_rate) \
    _rate>ONE_GiB? "GiB":    \
    _rate>ONE_MiB? "MiB":    \
    _rate>ONE_KiB? "KiB": "B"

using namespace ROS;
// Email conversation between Mikael Kvist and Gordon Crone. October 2016
//
// MK) I'm currently developing a new network-based plugin module. The idea is to
// support retrieval of detector calibration data over TCP. I have used code from
// EmulatedNPDescriptorModule.cxx [1] to implement a first working version. To test
// this I have created a python script that sends ROD-fragments over TCP on one port.
// The Level1ID is incremented by one for each new generated fragment. However, this
// module is restricted to only support one channel and without loss of TCP data (no
// loss of Level1IDs).
// GC) I am not sure what you are trying to do with multiple channels. Are you sending
// data from multiple network sources? With tcp, there should be no loss of data as
// the tcp protocol will retry if any packets are lost by the network. Maybe you
// mean you want to be resilient against the sender missing a trigger and just not
// sending a packet for a particular L1 Id? In this case, when the fragment is
// requested, you should create an empty fragment with the ROB header status set to
// 0x20000000 (lost) or 0x40000000 (pending) depending on whether you have received
// fragments with a later Id or not.

// References
// 1 ^ https://gitlab.cern.ch/atlas-tdaq-software/ROSEmulatedNP/tree/master/src
// 2 ^ https://gitlab.cern.ch/atlas-tdaq-software/ROSEmulatedNP/blob/master/src/EmulatedNPDescriptorModule.cxx#L427
// 3 ^ https://gitlab.cern.ch/atlas-tdaq-software/ROSEmulatedNP/blob/master/src/EmulatedNPDescriptorModule.cxx#L248
void TCPNPModule::configure(const daq::rc::TransitionCmd&)
{
    DEBUG_TEXT(DFDB_ROSFM, 15, "TCPNPModule::configure: Entering");

    m_server = new DataServer<TCPNPModule>(this);

    const daq::df::TCPNPModule* DAL =
        m_configurationDB->get<daq::df::TCPNPModule>(m_uid);

    unsigned int nPages = DAL->get_numOfPages();
    //      0       1..9        10        11             N-3..N
    // +---------+---------+---------+---------+ - - - +---------+
    // | LENGTH  | HEADER  |          ELEMENTS         | TRAILER |
    // +---------+---------+---------+---------+ - - - +---------+
    m_pageSize = (DAL->get_sizeOfEachPage() * ONE_KiB) + sizeof(u_int);
    m_maxNumOfFragments = (DAL->get_minimumSizeOfMemory() * ONE_MiB) / m_pageSize;
    m_heap = new TCPNPAlloc (m_maxNumOfFragments, m_pageSize);

    std::vector<const daq::core::ResourceBase*>::const_iterator channelIter =
        DAL->get_Contains().begin();
    // Email conversation between Mikael Kvist and Gordon Crone. October 2016
    //
    // MK) What would be the best way to modify my python script to test multiple
    // channels and data losses? Example: What do you think about implementing two
    // threads in the script where one sends even and the other one sends odd Level1IDs?
    // GC) If you need multiple channels, I would have a thread for each
    // sending (the same or different) data to a different port. For missing L1
    // Ids, I would just have the threads throw a random number and if it's
    // less than some threshold, skip an event.
    for (; channelIter != DAL->get_Contains().end(); channelIter++) {

        const daq::df::HW_InputChannel* channelPtr =
            m_configurationDB->cast<daq::df::HW_InputChannel, daq::core::ResourceBase> (*channelIter);

        if (channelPtr == 0) {
            ERS_LOG ("Module " << m_uid << " Contains relationship to something ("
                     << (*channelIter)->class_name() << ") that is not an HW_InputChannel");
            continue;
        }

        if (!(*channelIter)->disabled(*m_partition)) {
            unsigned int channelId = channelPtr->get_Id();
            std::string channelUID = channelPtr->UID();
            unsigned short port = channelPtr->get_PhysAddress();

            ERS_LOG ("Pushing back rolID "  << std::hex << channelId << std::dec
                      << ", UID=" << channelUID
                      << ", Port=" << port);

            m_channelUIDMap[channelUID] = channelId;
            m_UIDchannelMap[channelId] = channelUID;
            m_portMap[port].push_back(channelId);
            m_channelIds.push_back(channelId);
            m_enabledMap[channelId] = true;
            m_lastL1IDs[channelId] = ~0;
        }
    }
    initISInfo ();

    for (auto pair : m_portMap) {
        tcp::endpoint endpoint (tcp::v4(), pair.first);
        m_server->listenOn (endpoint);
    }

    m_enabled = new bool[m_channelIds.size()];
    for (unsigned int chnl = 0; chnl < m_channelIds.size(); chnl++) {
        m_enabled[chnl] = true;
    }

    for (; nPages--; ) {
        DataPage* page = new DataPage(NULL, NULL);
        m_pageQueue.push(page);
        m_virtualAddresses[page] = new unsigned int[NUMBER_OF_DATA_ELEMENTS];
    }

    DEBUG_TEXT(DFDB_ROSFM, 15, "TCPNPModule::configure: Leaving");
}

void TCPNPModule::registerChannels(
    unsigned int& baseIndex,
    std::vector<tbb::concurrent_bounded_queue<DataPage*>* > & pageQueueVec,
    std::vector<std::vector<uint32_t> >& channelIdVec)
{
    m_baseIndex = baseIndex;
    channelIdVec.push_back(m_channelIds);
    pageQueueVec.push_back(&m_pageQueue);

    for (auto id : m_channelIds) {
        m_channelIndex[id] = baseIndex;
        m_channelMap[baseIndex] = id;
        baseIndex++;
    }
}

void TCPNPModule::getL1CountPointers(
    std::vector<unsigned int*>& countPointers,
    std::vector<bool*>& enabled)
{
    for (auto& l1 : m_lastL1IDs) {
        countPointers.push_back((unsigned *)&(l1.second));
    }
    for (auto& en : m_enabledMap) {
        enabled.push_back (&(en.second));
    }
}

void TCPNPModule::prepareForRun (const daq::rc::TransitionCmd&)
{
    if (m_sockets.empty()) {
        m_server->run ();
    }

    auto it = m_lastL1IDs.begin();
    for (; it != m_lastL1IDs.end(); it++) {
        // GC) The variable should be initialised to 0xffffffff as the
        // NPRequestManager sees this as a 'not started' flag.
        it->second = ~0;
    }

    m_active = true;
    m_runThread = new std::thread(&TCPNPModule::runThread, this);

    if (m_sockets.empty()) {
        ::sleep (1);
        m_server->stop ();
    }

    m_tcpThread = new std::thread(&TCPNPModule::tcpThread, this);
}

void TCPNPModule::removeFromHeap (std::pair<unsigned,unsigned> keys, unsigned int* /*value*/)
{
    m_heap->dispose (remove_from_map<std::pair<unsigned,unsigned>,unsigned*> (keys, m_hash));
}

void TCPNPModule::stopGathering (const daq::rc::TransitionCmd&)
{
    ERS_LOG ("TCPNPModule::stopGathering() Waiting for threads to exit");

    m_active = false;
    m_cond.notify_all();
    m_messageQueue.abort();

    m_runThread->join ();
    m_tcpThread->join ();

    delete m_runThread;
    delete m_tcpThread;
    m_runThread = m_tcpThread = 0;

    ::sleep (1);
    walk_through_map<TCPNPModule,std::pair<unsigned,unsigned>,unsigned*> \
        (this,&TCPNPModule::removeFromHeap,m_hash);
    m_hash.clear ();
}

void TCPNPModule::unconfigure (const daq::rc::TransitionCmd&)
{
    deleteISInfo();
    if (m_enabled != 0) {
        delete[] m_enabled;
        m_enabled = 0;
    }
    for (auto socket : m_sockets) {
        socket->close();
        delete socket;
    }
    for (auto virtualAddress : m_virtualAddresses) {
        delete virtualAddress.first;
        delete virtualAddress.second;
    }
    for (auto& _vector: m_portMap) {
        _vector.second.clear();
    }
    m_virtualAddresses.clear ();
    m_pageQueue.clear ();
    m_channelIds.clear ();
    m_channelUIDMap.clear ();
    m_lastL1IDs.clear ();
    m_sockets.clear ();
    m_portMap.clear ();
    delete m_heap;
    delete m_server;
}

void TCPNPModule::disconnectOneSocket (unsigned int rodId)
{
    unsigned short port = ~0;

    // Resolve port number
    for (auto& _pair : m_portMap) {
        for (auto& _vector : _pair.second) {
            if (_vector == rodId) {
                port = _pair.first;
            }
        }
    }
    for (auto socket : m_sockets) {
        if (socket->is_open ())
        {
            if (socket->local_endpoint().port() == port)
            {
                boost::system::error_code errorcode;
                // Boost documentation recommends calling shutdown first
                // for "graceful" closing of socket.
                socket->shutdown(tcp::socket::shutdown_both, errorcode);
                if (errorcode)
                {
                    ERS_LOG ("socket->shutdown error: " << errorcode.message());
                }

                socket->close(errorcode);
                if (errorcode)
                {
                    ERS_LOG ("socket->close error: " << errorcode.message());
                }
            }
        }
    }
}

void TCPNPModule::enableOrDisable( const std::vector<std::string>& argVec, bool enable)
{
    for (std::string arg: argVec) {
        if (m_channelUIDMap.find(arg) != m_channelUIDMap.end()) {
            unsigned int rolID = m_channelUIDMap[arg];
            if (m_channelIndex.find(rolID) != m_channelIndex.end()) {
                unsigned int chan = m_channelIndex[rolID] - m_baseIndex;
                m_enabled[chan] = enable;
                ERS_LOG ((enable? "enabling": "disabling")
                         << " channel " << chan
                         << ", ROL ID " << std::hex
                         << rolID << std::dec);

                if (!enable) {
                    disconnectOneSocket (rolID);
                } else {
                    // Code for re-connect goes here ...
                }
            }
        }
    }
    for (unsigned int chan = 0; chan < m_channelIds.size(); chan++) {
        ERS_LOG ("channel: " << chan
                 << ", enabled= " << m_enabled[chan]);
    }
}

void TCPNPModule::initISInfo ()
{
    for (auto id : m_channelIds) {
        m_statistics.UID.push_back(m_UIDchannelMap[id]);
        m_statistics.rodId.push_back(id);
        m_statistics.level1RateHz.push_back(0);
        m_statistics.lastL1IdInput.push_back(~0);
        m_statistics.fragmentsServed.push_back(0);
        m_statistics.numberOfMayCome.push_back(0);
        m_statistics.numberOfNeverToCome.push_back(0);
        m_statistics.fragmentsCorrupted.push_back(0);
        m_statistics.fragmentsAvgLength.push_back(0);
        m_statistics.rxDataRate.push_back(rxDataRate(0,1.0));
        m_tmrs[id].reset();
        m_fragmentsServed[id] = 0;
        m_fragmentsPending[id] = 0;
        m_fragmentsLost[id] = 0;
        m_fragmentsCorrupted[id] = 0;
        m_numberOfLevel1Delta[id] = 0;
        m_avgLength[id] = 0;
        m_numOfSamples[id] = 0;
        m_numOfRxBytes[id] = 0;
    }
    { // do once
        m_statistics.errorsOccurred = 0;
        m_errorsOccurred = 0;
    }
}

ISInfo* TCPNPModule::getISInfo ()
{
    double elapsedTime;
    unsigned int i = 0;
    unsigned int numOfRxBytes;
    unsigned int numberOfLevel1Delta;
    for (auto id : m_channelIds) {
        elapsedTime = m_tmrs[id].elapsed();
        numberOfLevel1Delta = m_numberOfLevel1Delta[id];
        numOfRxBytes = m_numOfRxBytes[id];
        m_tmrs[id].reset();
        m_numberOfLevel1Delta[id] = 0;
        m_numOfRxBytes[id] = 0;
        m_statistics.level1RateHz[i] = (float)numberOfLevel1Delta/elapsedTime;
        m_statistics.lastL1IdInput[i] = m_lastL1IDs[id];
        m_statistics.fragmentsServed[i] = m_fragmentsServed[id];
        m_statistics.numberOfMayCome[i] = m_fragmentsPending[id];
        m_statistics.numberOfNeverToCome[i] = m_fragmentsLost[id];
        m_statistics.fragmentsCorrupted[i] = m_fragmentsCorrupted[id];
        m_statistics.fragmentsAvgLength[i] = m_avgLength[id];
        m_statistics.rxDataRate[i] = rxDataRate(numOfRxBytes, elapsedTime);
        i++;
    }
    { // do once
        m_statistics.errorsOccurred = m_errorsOccurred;
    }
    return &m_statistics;
}

void TCPNPModule::deleteISInfo ()
{
    {
        m_statistics.UID.clear();
        m_statistics.rodId.clear();
        m_statistics.level1RateHz.clear();
        m_statistics.lastL1IdInput.clear();
        m_statistics.fragmentsServed.clear();
        m_statistics.numberOfMayCome.clear();
        m_statistics.numberOfNeverToCome.clear();
        m_statistics.fragmentsCorrupted.clear();
        m_statistics.fragmentsAvgLength.clear();
        m_statistics.rxDataRate.clear();
        m_tmrs.clear();
        m_fragmentsServed.clear();
        m_fragmentsPending.clear();
        m_fragmentsLost.clear();
        m_fragmentsCorrupted.clear();
        m_numberOfLevel1Delta.clear();
        m_avgLength.clear();
        m_numOfSamples.clear();
        m_numOfRxBytes.clear();
    }
}

void TCPNPModule::clearInfo ()
{
    unsigned int i = 0;
    for (auto id : m_channelIds) {
        m_statistics.fragmentsServed[i] = 0;
        m_statistics.numberOfMayCome[i] = 0;
        m_statistics.numberOfNeverToCome[i] = 0;
        m_statistics.fragmentsCorrupted[i] = 0;
        m_statistics.fragmentsAvgLength[i] = 0;
        m_fragmentsServed[id] = 0;
        m_fragmentsPending[id] = 0;
        m_fragmentsLost[id] = 0;
        m_fragmentsCorrupted[id] = 0;
        m_numberOfLevel1Delta[id] = 0;
        m_avgLength[id] = 0;
        m_numOfSamples[id] = 0;
        m_numOfRxBytes[id] = 0;
        i++;
    }
    { // do once
        m_statistics.errorsOccurred = 0;
        m_errorsOccurred = 0;
    }
}

// Email conversation between Mikael Kvist and Gordon Crone. October 2016
//
// MK) It seems that for every channelIndex there's a status, NEW_REQUEST,
// REQUEST_PENDING and REQUEST_COMPLETE. For example:
// descriptor->status(channelIndex,NPRequestDescriptor::RequestStatus::REQUEST_COMPLETE); [2]
// What is their purpose and when should they be used?
// GC) The request status in the descriptor was used by the request
// management to handle retries. We don't currently use this mechanism as
// we don't issue the request until we know the data have arrived or we
// have time out.
// The descriptor should have status NEW_REQUEST for every channel
// when it is passed to the module's requestFragment(). You should set it
// to REQUEST_COMPLETE for each channel that you have supplied data for.
// For channels which you have generated an empty fragment with status
// 0x40000000, you can set the request status to REQUEST_PENDING but we
// since we don't do re-requests at the moment, so you could set these to
// REQUEST_COMPLETE as well.
void TCPNPModule::channelRequestFragment (
    unsigned int channelIndex,
    NPRequestDescriptor* descriptor)
{
    std::unique_lock<std::mutex> lk(m_mtx);
    unsigned int rolId = m_channelMap[channelIndex];
    unsigned int level1Id = descriptor->level1Id();

    ROBFragment::ROBHeader* robHeader=
        (ROBFragment::ROBHeader*)descriptor->headerPage(channelIndex)->data();

    robHeader->generic.startOfHeaderMarker = EventFragment::s_robMarker;
    unsigned int headerSize =
        (sizeof(ROBFragment::ROBHeader) / sizeof(u_int))
        -ROBFragment::s_nStatusElements + m_numberOfStatusElements;
    robHeader->generic.headerSize = headerSize;
    robHeader->generic.sourceIdentifier = rolId;
    robHeader->generic.formatVersionNumber = EventFragment::s_formatVersionNumber;
    robHeader->generic.numberOfStatusElements = m_numberOfStatusElements;

    DataPage* page;
    if (descriptor->status(channelIndex)==NPRequestDescriptor::RequestStatus::NEW_REQUEST) {
        try {
            m_pageQueue.pop(page);
            descriptor->assignPage(channelIndex,page);
        } catch (...) {
            ERS_LOG ("Exception from page queue");
            return;
        }
    }
    else {
        page = descriptor->dataPage(channelIndex);
    }

    unsigned int* virtualAddress = m_virtualAddresses[page];
    RODFragment::RODHeader* rodHeader=(RODFragment::RODHeader*) virtualAddress;
    rodHeader->startOfHeaderMarker=EventFragment::s_rodMarker;
    rodHeader->headerSize=RODFragment::s_rodheaderSize;
    rodHeader->formatVersionNumber=FMT_VERSION_NUM;
    rodHeader->sourceIdentifier=rolId;
    rodHeader->level1Id=level1Id;
    rodHeader->detectorEventType=0;
    RODFragment::RODTrailer* rodTrailer=
        (RODFragment::RODTrailer*) &virtualAddress[rodHeader->headerSize];
    rodTrailer->numberOfStatusElements=0;
    rodTrailer->numberOfDataElements=0;
    rodTrailer->statusBlockPosition=0;

    unsigned int* rodFragment = NULL;
    unsigned int  numberOfDataElements = NUMBER_OF_DATA_ELEMENTS;

    if (m_enabled[channelIndex - m_baseIndex]) {
        rodFragment = find_in_map<std::pair<unsigned,unsigned>,unsigned*>\
            (std::make_pair(level1Id,rolId), m_hash);
        if (rodFragment != NULL) {
            //      0       1..9        10        11             N-3..N
            // +---------+---------+---------+---------+ - - - +---------+
            // | LENGTH  | HEADER  |          ELEMENTS         | TRAILER |
            // +---------+---------+---------+---------+ - - - +---------+
            numberOfDataElements = rodFragment[0];
            // Replacement new
            page = new (page) DataPage (&rodFragment[1], NULL);

            for (unsigned int word = 0; word < m_numberOfStatusElements; word++) {
                robHeader->statusElement[word] = 0;
            }
            descriptor->status(channelIndex, NPRequestDescriptor::RequestStatus::REQUEST_COMPLETE);
            m_fragmentsServed[rolId] ++;
        }

        else {
            page = new (page) DataPage (virtualAddress, NULL);
            if (level1Id > m_lastL1IDs[rolId]) {
                robHeader->statusElement[0] = STATUS_PENDING;
                if (m_numberOfStatusElements > 1) {
                    robHeader->statusElement[1] = m_lastL1IDs[rolId];
                }
                descriptor->status(channelIndex, NPRequestDescriptor::RequestStatus::REQUEST_PENDING);
                m_fragmentsPending[rolId] ++;
            }
            else {
                descriptor->status(channelIndex, NPRequestDescriptor::RequestStatus::REQUEST_COMPLETE);
                robHeader->statusElement[0] = STATUS_LOST;
                m_fragmentsLost[rolId] ++;
            }
        }
    }

    else {
        page = new (page) DataPage (virtualAddress, NULL);
        descriptor->status(channelIndex, NPRequestDescriptor::RequestStatus::REQUEST_COMPLETE);
        robHeader->statusElement[0] = 0x80000000;
    }

    robHeader->generic.totalFragmentsize =
        robHeader->generic.headerSize + numberOfDataElements;

    lk.unlock();
}

std::string TCPNPModule::rxDataRate(unsigned int numOfRxBytes, double elapsedTime)
{
    static char stringout[1024];
    double dataRate = numOfRxBytes / elapsedTime;
    const char* binaryPrefix = BINARY_PREFIX(dataRate);
    if      (dataRate > ONE_GiB) dataRate /= ONE_GiB;
    else if (dataRate > ONE_MiB) dataRate /= ONE_MiB;
    else if (dataRate > ONE_KiB) dataRate /= ONE_KiB;
    snprintf (stringout, sizeof (stringout), "%.3f %s/s", dataRate, binaryPrefix);
    return std::string (stringout);
}

int TCPNPModule::processMemUsage()
{
    int tSize = 0, resident = 0, share = 0;
    std::ifstream buffer("/proc/self/statm");
    buffer >> tSize >> resident >> share;
    buffer.close();
    // in case x86-64 is configured to use 2MB pages
    long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024;
    double rss = resident * page_size_kb;
    ERS_LOG ("RSS - " << rss << " kB");
    double shared_mem = share * page_size_kb;
    ERS_LOG ("Shared memory - " << shared_mem << " kB");
    ERS_LOG ("Private memory - " << rss - shared_mem << " kB");
    return 0;
}

// Email conversation between Mikael Kvist and Gordon Crone. October 2016
//
// MK) I have a question about the dump and user function in EmulatedNPDescriptorModule.cxx,
// what do they do and when are they called? (I can see that the dump is called by the
// user-function). In my current TCP implementation I have left dump and user "empty".
// GC) You do not need to implement or even declare either of these functions unless you
// have something specific you want to do. The dump function is just there for debugging.
// The user function is part of the run control interface. You can implement anything you
// want in there and it will be called whenever a user command is sent from eg. rc_sender.
void TCPNPModule::dump()
{
    ( void )processMemUsage ();
}

void TCPNPModule::requestFragment(NPRequestDescriptor* descriptor)
{
    auto list = descriptor->localIds();
    if (list->size() != 0) {
        bool last = false;
        for (unsigned int index = 0; index < list->size(); index++) {
            if (index == list->size() - 1) {
                last = true;
            }
            queueRequest(descriptor, (*list)[index], last);
        }
    }
    descriptor->expectReply(1);
}

void TCPNPModule::clearFragments(std::vector<uint32_t>& level1Ids)
{
    {
        std::unique_lock<std::mutex> lk(m_mtx);
        for (auto l1 : level1Ids)
            for (auto id : m_channelIds)
                removeFromHeap (std::make_pair (l1, id), NULL);
    }
    m_cond.notify_one();
}

void TCPNPModule::runThread()
{
    unsigned int nRequests = 0;
    while (m_active) {
        try {
            Message* message;
            m_messageQueue.pop(message);
            if (message) {
                channelRequestFragment(message->channel(),message->descriptor());

                if (message->is_last()) {
                    // Email conversation between Mikael Kvist and Gordon Crone.
                    // October 2016
                    //
                    // MK) How should m_collectorQueue->push be used in a multiple
                    // channel system?
                    // GC) You should only push to m_collectorQueue when you have
                    // processed the last channel.
                    if (m_collectorQueue!=0) {
                        nRequests ++;
                        m_collectorQueue->push(message->descriptor());
                    }
                }
                delete message;
            }
        }

        catch (tbb::user_abort& abortException) {
            ERS_LOG ("TCPNPModule: run thread aborting...");
            m_active = false;
        }
    }
    ERS_LOG ("TCPNPModule handled " << nRequests << " requests");
}


void TCPNPModule::tcpThread()
{
    int return_value;
    int poll_timeout_ms = 0;

    struct pollfd pollfd;
    // Descriptor for the poll operation
    // Instructs poll to check for available data on
    // on the socket
    memset (&pollfd, '\0', sizeof pollfd);
    pollfd.events |= POLLIN;

    std::vector<tcp::socket *>::iterator it =
            m_sockets.begin ();

    while (m_active) {
        if (it == m_sockets.end()) break;

        bool success = true;
        tcp::socket* socket = dynamic_cast<tcp::socket*>(*it);

        std::unique_lock<std::mutex> lk(m_mtx);
        m_cond.wait(lk, [this]{return (m_hash.size() < m_maxNumOfFragments) || !m_active;});

        if (m_active == false) {
            lk.unlock();
            break;
        }

        pollfd.fd = socket->native_handle ();
        // With poll we check if data are available
        // before reading
        // This is needed to avoid blocking forever
        return_value = poll (&pollfd, 1, poll_timeout_ms);

        if (return_value > 0)
        {
            // On success, a positive number is returned; this is the number of
            // structures which have nonzero revents fields.
            success = receive (socket);
        }
        else if (return_value < 0)
        {
            // On error, -1 is returned.
            m_errorsOccurred ++;
        }
        else
        {
            // A value of 0 indicates that the call timed out and no file descriptors
            // were ready.

            // Do nothing.
        }

        lk.unlock();


        if (success == false)
        {
            ERS_LOG ("Error: Connection closed cleanly by peer (?)");
            socket->close ();
            it = m_sockets.erase (it);
            delete socket;
        } else it ++;


        // Cyclic iterator
        if (it == m_sockets.end())
        {
            it = m_sockets.begin();
            // Allowing other threads to run.
            std::this_thread::yield();
        }
    }

    ERS_LOG ("TCPNPModule: TCP thread finished...");
}

bool TCPNPModule::isCorrupt (RODFragment::RODHeader* rodHeader, RODFragment::RODTrailer* rodTrailer, unsigned int numberOfWordElements)
{
    if ( (RODFragment::s_rodheaderSize + RODFragment::s_rodtrailerSize) > numberOfWordElements )
    {
        // fragment is shorter that 12 words
        return true;
    }
    if ( (RODFragment::s_rodheaderSize + rodTrailer->numberOfDataElements + RODFragment::s_rodtrailerSize + rodTrailer->numberOfStatusElements) !=
        numberOfWordElements )
    {
        // size not matching header + data + trailer + status words
        // as indicated in trailer
        return true;
    }
    if ( EventFragment::s_rodMarker != rodHeader->startOfHeaderMarker )
    {
        // fragment with faulty ROD marker
        return true;
    }
    if ( m_UIDchannelMap.find(rodHeader->sourceIdentifier) == m_UIDchannelMap.end() )
    {
        // Unknown Source Identifier
        return true;
    }

    return false;
}

bool TCPNPModule::receive (tcp::socket* socket)
{
    auto search = m_portMap.find(socket->local_endpoint().port());
    auto _vector = search->second;
    boost::system::error_code error;

    // Receive the size of message
    unsigned int length;
    boost::asio::read (*socket, boost::asio::buffer(&length, sizeof length), error);

    if (!error)
    {
        //      0       1..9        10        11             N-3..N
        // +---------+---------+---------+---------+ - - - +---------+
        // | LENGTH  | HEADER  |          ELEMENTS         | TRAILER |
        // +---------+---------+---------+---------+ - - - +---------+
        unsigned int* rodFragment = (unsigned int *)m_heap->alloc (length + sizeof length);
        // save size of ROD fragment
        rodFragment[0] = length/sizeof(uint);
        // Receive the full message
        boost::asio::read (*socket, boost::asio::buffer (&rodFragment[1], length), error);

        if (!error)
        {
            RODFragment::RODHeader* rodHeader =
                (RODFragment::RODHeader*)&rodFragment[1];
            RODFragment::RODTrailer* rodTrailer =
                (RODFragment::RODTrailer*)&rodFragment[1 + rodFragment[0] - RODFragment::s_rodtrailerSize];

            unsigned int rolId = _vector.size() > 1? rodHeader->sourceIdentifier: _vector[0];
            unsigned int level1Id = rodHeader->level1Id;

            // returns true if the RODFragment is corrupted.
            if (isCorrupt(rodHeader, rodTrailer, rodFragment[0]))
            {
                m_fragmentsCorrupted[rolId]++;
                m_heap->dispose(rodFragment);
            }
            else
            {
                m_numberOfLevel1Delta[rolId] ++;
                m_numOfRxBytes[rolId] += length;
                // Email conversation between Mikael Kvist and Gordon Crone. October 2016
                //
                // MK) In getL1CountPointers [3] we are populating the countPointers with
                // &m_lastl1. As I understand it, in a multi-channel system we need one m_lastl1
                // for each channel. So far so good.
                // In EmulatedNPDescriptorModule.cxx the m_lastl1 has a fixed value, and in
                // the TCP-module, I guess m_lastl1 can not. Am I supposed to increment m_lastl1
                // when a new fragment is received for that channel? And if so, should m_lastl1
                // have starting value 0.
                // GC) In a proper multichannel setup, you will have to keep an index for
                // each channel so it is best to have a class for the channel and have an
                // m_lastl1 as a member of that class rather than the module class. The
                // variable should be initialised to 0xffffffff as the NPRequestManager
                // sees this as a 'not started' flag.
                m_lastL1IDs[rolId] = level1Id;
                add_to_map<std::pair<unsigned,unsigned>,unsigned*> \
                    (std::make_pair(level1Id,rolId), rodFragment, m_hash);

                // Handle overflow
                if (m_numOfSamples[rolId] + 1 < 1) m_numOfSamples[rolId] = m_avgLength[rolId] = 0;
                // Calculate moving average
                m_avgLength[rolId] = approxRollingAverage (m_avgLength[rolId], length, ++m_numOfSamples[rolId]);
            }
        }
        else {
            m_heap->dispose(rodFragment);
        }
    }

    return error? false: true;
}

//FOR THE PLUGIN FACTORY
extern "C" {
    extern ReadoutModule* createTCPNPModule();
}
ReadoutModule* createTCPNPModule(){
    return (new TCPNPModule());
}

// EOF
