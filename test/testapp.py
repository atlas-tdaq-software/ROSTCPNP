#!/usr/bin/env tdaq_python
#title          :testapp.py
#description    :Send ROD fragments
#author         :mkvist (mikael.stefan.kvist@cern.ch)
#date           :2016.9.20
#version        :0.1
#usage          :tdaq_python testapp.py [-h] [-r ROD_ID] [-i TCP_IP] [-p TCP_PORT]
#note           :See documentation: https://edms.cern.ch/document/445840/5.0a
#=================================================================================
import socket, time
from struct import pack, calcsize
from ctypes import c_uint32, Structure, Union, sizeof

HERTZ = lambda s: 1/s
USLEEP = lambda x: time.sleep(x/1000000.0)
MKSTRING = lambda x: "".join(map(chr, (ord('a')+(y%26) for y in range(x))))

class FormatVersionNumber(Structure): # pylint: disable=too-few-public-methods
    """
    Byte |      3      |      2      |      1      |      0      |
         +-------------+-------------+-------------+-------------+
         |   Major version number    |   Minor version number    |
         +---------------------------+---------------------------+ """
    _fields_ = [('Minor', c_uint32, 16), ('Major', c_uint32, 16)]

class FormatVersionNumberU(Union): # pylint: disable=too-few-public-methods
    """Format version number Union"""
    _fields_ = [('Bits', FormatVersionNumber), ('Word', c_uint32)]

class SourceIdentifier(Structure): # pylint: disable=too-few-public-methods
    """
    Byte |      3      |      2      |      1      |      0      |
         +-------------+-------------+-------------+-------------+
         |Optional=0x00|SubDetectorID|         Module ID         |
         +-------------+-------------+---------------------------+ """
    _fields_ = [('ModuleId', c_uint32, 16),
                ('SubDetectorId', c_uint32, 8),
                ('Optional', c_uint32, 8)]

class SourceIdentifierU(Union): # pylint: disable=too-few-public-methods
    """Source identifier Union"""
    _fields_ = [('Bits', SourceIdentifier), ('Word', c_uint32)]

class Header(Structure): # pylint: disable=too-few-public-methods
    """ROD data format
    +------------------------+
    | Start of Header Marker |
    +------------------------+
    |      Header size       |
    +------------------------+
    | Format version number  |
    +------------------------+
    |   Source identifier    |
    +------------------------+
    |       Run number       |
    +------------------------+
    |  Extended Level 1 ID   |
    +------------------------+
    |   Bunch crossing ID    |
    +------------------------+
    |  Level 1 trigger type  |
    +------------------------+
    |  Detector event type   |
    +------------------------+ """
    _fields_ = [
        ('StartOfHeader', c_uint32),
        ('HeaderSize', c_uint32),
        ('FmtVersionNumber', FormatVersionNumberU),
        ('SourceId', SourceIdentifierU),
        ('RunNumber', c_uint32),
        ('ExtendedLevel1Id', c_uint32),
        ('BunchCrossingId', c_uint32),
        ('Level1TriggerType', c_uint32),
        ('DetectorEventType', c_uint32)]

class Trailer(Structure): # pylint: disable=too-few-public-methods
    """
    +---------------------------+
    | Number of status elements |
    +---------------------------+
    |  Number of data elements  |
    +---------------------------+
    |   Status block position   |
    +---------------------------+ """
    _fields_ = [
        ('NoOfStatusElem', c_uint32),
        ('NoOfDataElem', c_uint32),
        ('StatusBlockPos', c_uint32)]

def main(rodId, host, port):
    """main starts here"""
    header = Header()
    header.StartOfHeader = 0xee1234ee # ROD
    header.HeaderSize = 9
    header.FmtVersionNumber.Bits.Major = 0x0301 # 3.1
    if rodId == 0:
        header.SourceId.Bits.SubDetectorId = 0x11 # Pixel->Barrel
        header.SourceId.Bits.ModuleId = 1
    else:
        header.SourceId.Word = rodId

    dataelements = bytes(MKSTRING(244 * sizeof(c_uint32)))

    trailer = Trailer()
    trailer.NoOfDataElem = len(dataelements)/sizeof(c_uint32)
    trailer.NoOfStatusElem = 0

    # Socket stuff
    sock = socket.socket()
    sock.connect((host, port))
    # Format: Header + Data + Trailer
    fmt = '9I%ds3I' % len(dataelements)

    while True:
        # start_time = time.time()
        sizeofstream = pack('I', calcsize(fmt)) # size in bytes
        stream = pack(
            fmt,
            header.StartOfHeader,
            header.HeaderSize,
            header.FmtVersionNumber.Word,
            header.SourceId.Word,
            header.RunNumber,
            header.ExtendedLevel1Id,
            header.BunchCrossingId,
            header.Level1TriggerType,
            header.DetectorEventType,
            dataelements,
            trailer.NoOfStatusElem,
            trailer.NoOfDataElem,
            trailer.StatusBlockPos)
        sock.sendto(sizeofstream, (host, port))
        sock.sendto(stream, (host, port))

        header.ExtendedLevel1Id += 1
        # print("--- %s Hertz ---" % HERTZ(time.time() - start_time))
        # raw_input("Press Enter to continue...")

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument("-r", "--rod_id", help="RodId")
    parser.add_argument("-i", "--tcp_ip", help="Destination")
    parser.add_argument("-p", "--tcp_port", help="Port Number")

    args = parser.parse_args()

    rod_id = int(args.rod_id) if args.rod_id else 0
    tcp_ip = args.tcp_ip if args.tcp_ip else 'localhost'
    tcp_port = int(args.tcp_port) if args.tcp_port else 1234

    main(rod_id, tcp_ip, tcp_port)

# EOF
