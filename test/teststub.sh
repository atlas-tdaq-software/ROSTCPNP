#!/bin/bash

TCP_PORT=1234
case $1 in
    ''|*[!0-9]*) ;;
    *) TCP_PORT=$1 ;;
esac

echo -e "\nHex dump (PORT $TCP_PORT)\n"
nc -lk $TCP_PORT | hexdump -e '"%04.4_ax " 4/1 " %02X" "  |"' -e '/1 "%_p"' -e '/4 "|\n"'

# EOF