#!/bin/bash
set -m
(
    tdaq_python testapp.py -i pc-tbed-pub-10.cern.ch -p 1234 &
    child=$!
    trap -- "" SIGTERM
    (
        tdaq_python testapp.py -i pc-tbed-pub-10.cern.ch -p 6767
        kill $child
    )
)
