#ifndef TCPNPDATASERVER_H
#define TCPNPDATASERVER_H
#include<thread>
#include<boost/asio.hpp>
using boost::asio::ip::tcp;

namespace ROS {
    template <class C>
    class DataServer {
    public :
        DataServer (C* who) : m_who(who), m_numfds(0), m_io_service(), m_running(false) { };
        ~DataServer () ;
        void listenOn (tcp::endpoint const&);
        void run ();
        void stop ();
    private :
        void work ();
        C* m_who;
        unsigned int m_numfds;
        std::vector<tcp::acceptor*> m_acceptors;
        std::shared_ptr<std::thread> m_thread;
        boost::asio::io_service m_io_service;
        volatile std::atomic<bool> m_running;
    };
#include"src/TCPNPDataServer.cxx"
}

#endif//TCPNPDATASERVER_H
