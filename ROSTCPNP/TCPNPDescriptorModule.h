#ifndef TCPNPDESCRIPTORMODULE_H
#define TCPNPDESCRIPTORMODULE_H
#include<mutex>
#include<atomic>
#include<stdlib.h>
#include<condition_variable>
#include"tbb/concurrent_hash_map.h"
#include"ROSDescriptorNP/NPRequestDescriptor.h"
#include"ROSDescriptorNP/NPReadoutModule.h"
#include"ROSEventFragment/ROBFragment.h"
#include"ROSTCPNP/TCPNPDataServer.h"
#include"ROSTCPNP/TCPNPAlloc.h"
#include"ROSInfo/TCPNPInfo.h"

namespace ROS {
    class TCPNPModule : public NPReadoutModule {

    // Declare a friend class
    template <class C> friend class DataServer;

    public :
        // Constructor
        TCPNPModule() : m_numberOfStatusElements(2), m_enabled(0) { };
        // Destructor
        virtual ~TCPNPModule() noexcept { };

        // FSM
        // Instantiate and configure TCPNP object
        virtual void configure(const daq::rc::TransitionCmd&);
        // Initialize variables and starts two threads
        virtual void prepareForRun(const daq::rc::TransitionCmd&);
        // Disables and stops all threads
        virtual void stopGathering(const daq::rc::TransitionCmd&);
        // De-configure and delete TCPNP object
        virtual void unconfigure(const daq::rc::TransitionCmd&);

        // Descriptor channel setup method
        void registerChannels (
            unsigned int& baseIndex,
            std::vector<tbb::concurrent_bounded_queue<DataPage*>* > & pageQueueVec,
            std::vector<std::vector<uint32_t> >& channelIdVec);
        virtual void getL1CountPointers (
            std::vector<unsigned int*>& countPointers,
            std::vector<bool*>& enabled);

        // Place data channels in discard mode
	virtual void disable(const std::vector<std::string>& argVec) {
            enableOrDisable(argVec, false); };
	// Take data channels out of discard mode
	virtual void enable(const std::vector<std::string>& argVec) {
            enableOrDisable(argVec, true); };

        // Fragment request method
        virtual void requestFragment(NPRequestDescriptor* descriptor);
        // Fragment deletion method
        virtual void clearFragments(std::vector<uint32_t>& level1Ids);

        // IS info collection method
        ISInfo* getISInfo ();
        // Clear all data driven trigger statistics
	virtual void clearInfo ();
        // Get the list of channels connected to this module
        virtual const std::vector<DataChannel *> *channels () { return 0; };

        void dump();
        int processMemUsage();
        virtual void user(const daq::rc::UserCmd& cmd) {
            if (cmd.commandName() == "dump") dump(); };

    private :
        class Timer {
        public:
        Timer() : beg_(clock_::now()) {}
            void reset() { beg_ = clock_::now(); }
            double elapsed() const {
                return std::chrono::duration_cast<second_>
                    (clock_::now() - beg_).count(); }
        private:
            typedef std::chrono::high_resolution_clock clock_;
            typedef std::chrono::duration<double, std::ratio<1> > second_;
            std::chrono::time_point<clock_> beg_;
        };
        class Message {
        public :
            Message(NPRequestDescriptor* request, unsigned int chan, bool isLast)
                : m_descriptor(request), m_channel(chan), m_isLast(isLast) { };
            NPRequestDescriptor* descriptor() {return m_descriptor;};
            unsigned int channel() {return m_channel;};
            bool is_last() {return m_isLast;};
        private :
            NPRequestDescriptor* m_descriptor;
            unsigned int m_channel;
            bool m_isLast;
        };
        DataServer<TCPNPModule>* m_server;

        TCPNPAlloc* m_heap;
        TCPNPInfo m_statistics;

        tbb::concurrent_hash_map<std::pair<unsigned, unsigned>, unsigned *> m_hash;
        tbb::concurrent_bounded_queue<Message *> m_messageQueue;
        tbb::concurrent_bounded_queue<DataPage *> m_pageQueue;

        std::map<unsigned short, std::vector<unsigned int> > m_portMap;
        std::map<DataPage *, unsigned int *> m_virtualAddresses;
        std::map<std::string,  unsigned int> m_channelUIDMap;
        std::map<unsigned int, std::string> m_UIDchannelMap;
        std::map<unsigned int, std::atomic_uint> m_lastL1IDs;
        std::map<unsigned int, unsigned int> m_channelIndex;
        std::map<unsigned int, unsigned int> m_channelMap;
        std::map<unsigned int, tcp::socket*> m_socketMap;
        std::map<unsigned int, bool> m_enabledMap;

        std::thread* m_runThread;
        std::thread* m_tcpThread;
        std::mutex m_mtx;

        std::vector<unsigned int> m_channelIds;
        std::vector<tcp::socket *> m_sockets;

        std::atomic_ulong m_maxNumOfFragments;
        std::condition_variable m_cond;

        std::string rxDataRate (unsigned int, double);

        unsigned int m_numberOfStatusElements;
        unsigned int m_baseIndex;
        unsigned int m_pageSize;

        bool isCorrupt (
            RODFragment::RODHeader*,
            RODFragment::RODTrailer*,
            unsigned int);
        bool  receive (tcp::socket*);
        bool* m_enabled;
        bool  m_active;

        void queueRequest(NPRequestDescriptor* request, unsigned int chanId, bool last) {
            m_messageQueue.push(new Message(request, chanId, last)); };
        void removeFromHeap (std::pair<unsigned,unsigned>, unsigned int*);
        void channelRequestFragment(unsigned int, NPRequestDescriptor*);
        void enableOrDisable(const std::vector<std::string>&, bool);
        void disconnectOneSocket (unsigned int);
        void deleteISInfo ();
        void initISInfo ();
        // Threads
        void runThread();
        void tcpThread();

        // Statistics
        std::map<unsigned int, Timer> m_tmrs;                          // timers
        std::map<unsigned int, std::atomic_ullong> m_fragmentsServed;  // # of fragments served
        std::map<unsigned int, std::atomic_uint> m_fragmentsPending;   // # of MayCome (= pending) fragments
        std::map<unsigned int, std::atomic_uint> m_fragmentsLost;      // # of NeverToCome (= lost) fragments
        std::map<unsigned int, std::atomic_uint> m_fragmentsCorrupted; // # of corrupted fragments
        std::map<unsigned int, std::atomic_uint> m_numberOfLevel1Delta;
        std::map<unsigned int, std::atomic_uint> m_avgLength;          // rolling average length of fragments
        std::map<unsigned int, std::atomic_ushort> m_numOfSamples;
        std::map<unsigned int, std::atomic_uint> m_numOfRxBytes;       // # of bytes received
        std::atomic_uint m_errorsOccurred;                             // # of errors occurred in poll()
    };

    double approxRollingAverage (double avg, double newSample, unsigned int numOfSamples) {
        avg -= avg / numOfSamples;
        avg += newSample / numOfSamples;
        return avg;
    }

    // Description:
    //
    // A concurrent_hash_map maps keys to values in a way that permits multiple
    // threads to concurrently access values. The keys are unordered.

    // insert a key-value pair
    template <typename K, typename V>
    void add_to_map(K key, V val, tbb::concurrent_hash_map<K,V>& m) {
        typename tbb::concurrent_hash_map<K,V>::accessor a;
        m.insert(a,key);
        a->second = val;
        a.release();
    }

    // retrieve a key-value pair
    template <typename K, typename V>
    V find_in_map(K key, tbb::concurrent_hash_map<K,V>& m) {
        V v = V();
        {
            typename tbb::concurrent_hash_map<K,V>::const_accessor a;
            if (m.find(a,key))
                v = a->second;
        }
        return v;
    }

    // remove a key-value pair
    template <typename K, typename V>
    V remove_from_map(K key, tbb::concurrent_hash_map<K,V>& m) {
        V v = V();
        {
            typename tbb::concurrent_hash_map<K,V>::accessor a;
            if (m.find(a,key)) {
                v = a->second;
                m.erase(a);
            }
        }
        return v;
    }

    // traverse
    template <class C, typename K, typename V>
    void walk_through_map (C* callee, void (C::*callback)(K, V), tbb::concurrent_hash_map<K,V>& m) {
        typename tbb::concurrent_hash_map<K,V>::iterator cursor, next;
        for (cursor = next = m.begin(); cursor != m.end(); cursor = next) {
            next++;
            (callee->*callback)(cursor->first, cursor->second);
        }
    }
}

#endif//TCPNPDESCRIPTORMODULE_H
