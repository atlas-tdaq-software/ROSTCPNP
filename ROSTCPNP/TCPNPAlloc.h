#ifndef TCPNPALLOC_H
#define TCPNPALLOC_H
#include<mutex>

namespace ROS {
    class TCPNPAlloc {
    private :
        // The purpose of the structure's definition is tha
        // we can operate linkedlist conveniently.
        typedef struct node {
            struct node* prev,* next;
        } node_t;

        // The address of the fixed-size blocks allocation
        char* m_memStart;

        // Manage all nodes with two linkedlists.
        // Head pointer to allocated linkedlist.
        node_t* m_allocatedList;
        // Head pointer to freed linkedlist.
        node_t* m_freedList;

        // Fixed-size blocks allocation size
        unsigned long m_sizeOfMem;
        // Memory node size
        unsigned long m_sizeOfEachElement;

        // Utility functions
        void push (node_t**, node_t*);
        node_t* deleteNode (node_t**, node_t*);

        std::mutex m_mtx;
    public:
        TCPNPAlloc (unsigned long numOfElements, unsigned long sizeOfEachElement);
        // Destructor of this class. It's task is to delete the
        // fixed-size blocks allocation.
        ~TCPNPAlloc () { delete[] m_memStart; };

        // Allocate memory node
        void* alloc (unsigned long size);
        // Free memory node.
        void dispose (void* p);
        // Walk through
        template <class C> void traverse (C* callee, void (C::*callback)(void*)) {
            node_t* cursor,* next;
            for (cursor = m_allocatedList; cursor; cursor = next) {
                next = cursor->next;
                (callee->*callback)((void *)(cursor + 1));
            }
        }
    };
}

#endif//TCPNPALLOC_H
